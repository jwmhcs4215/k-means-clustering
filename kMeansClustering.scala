import org.apache.spark.mllib.clustering.{KMeans, KMeansModel}
import org.apache.spark.mllib.linalg.Vectors

val inp = sc.textFile("kmeans_inp.txt")
val convert_inp = inp.map(s => Vectors.dense(s.split(' ').map(_.toDouble))).cache()

val num_clusters = 4
val num_iter = 50
val final_clusters = KMeans.train(convert_inp, num_clusters, num_iter)

val WSSSE = final_clusters.computeCost(convert_inp)
println(s"WSSSE Error = $WSSSE")

System.exit(0)