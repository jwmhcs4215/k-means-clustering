# K Means Clustering

Understood how to implement basic K means clustering algo using ML library of apache spark.

WSSSE = Within Set Sum of Squared Errors
The standard KMeans algorithm aims at minimizing the sum of squares of the distance between the points of each set: the squared Euclidean distance. This is the WCSS objective. Once you have computed the val result = KMeans.train(<trainData>, <clusterNumber>, <Iterations>) you can evaluate the result by using Within Set Sum of Squared Errors (something like the sum of the distances of each observation in each K partition) :
